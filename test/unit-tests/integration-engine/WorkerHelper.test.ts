import WorkerHelper from "#ie/WorkerHelper";
import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Message } from "amqplib";
import { expect } from "chai";

describe("WorkerHelper - parse message", () => {
    const fakeFullMessage = {
        content: Buffer.from(`{"from":"2022-03-10T00:00:00Z","to":"2022-03-14T00:00:00Z","type":"sddr"}`, "utf-8"),
    } as Message;
    const fakeFullMessageUnknownType = {
        content: Buffer.from(`{"from":"2022-03-10T00:00:00Z","to":"2022-03-14T00:00:00Z","type":"xxx"}`, "utf-8"),
    } as Message;
    const fakeFullMessageMissingType = {
        content: Buffer.from(`{"from":"2022-03-10T00:00:00Z","to":"2022-03-14T00:00:00Z"}`, "utf-8"),
    } as Message;
    const fakeEmptyMessage = {
        content: Buffer.from("", "utf-8"),
    } as Message;
    const fakeInvalidMessage = {
        content: Buffer.from(`{"type":"sddr"}`, "utf-8"),
    } as Message;
    const fakeInvalidMessage2 = {
        content: Buffer.from(`{"from":"2022-03-10T00:00:00Z"}`, "utf-8"),
    } as Message;
    const fakeInvalidMessage3 = {
        content: Buffer.from(`abcdefg`, "utf-8"),
    } as Message;
    const fakeInvalidMessage4 = {
        content: Buffer.from(`{"from":"2022-03-15T00:00:00Z","to":"2022-03-14T00:00:00Z","type":"sddr"}`, "utf-8"),
    } as Message;

    it("should parse message with type, from, to values", async () => {
        const parsedMessage = WorkerHelper.parseMessage(fakeFullMessage);
        expect(parsedMessage.from?.getTime()).equals(DateTime.fromISO("2022-03-10T00:00:00Z").toJSDate().getTime());
        expect(parsedMessage.to?.getTime()).equals(DateTime.fromISO("2022-03-14T00:00:00Z").toJSDate().getTime());
        expect(parsedMessage.type).equals(AggregationTaskType.SDDR);
    });

    it("should throw error for message with unknown type", async () => {
        expect(() => WorkerHelper.parseMessage(fakeFullMessageUnknownType)).to.throw(GeneralError);
    });

    it("should throw error for message with missing type", async () => {
        expect(() => WorkerHelper.parseMessage(fakeFullMessageMissingType)).to.throw(GeneralError);
    });

    it("should parse empty message", async () => {
        const parsedMessage = WorkerHelper.parseMessage(fakeEmptyMessage);
        expect(parsedMessage.from).to.be.undefined;
        expect(parsedMessage.to).to.be.undefined;
        expect(parsedMessage.type).to.be.undefined;
    });

    it("should throw error for invalid message #1", async () => {
        expect(() => WorkerHelper.parseMessage(fakeInvalidMessage)).to.throw(GeneralError);
    });

    it("should throw error for invalid message #2", async () => {
        expect(() => WorkerHelper.parseMessage(fakeInvalidMessage2)).to.throw(GeneralError);
    });

    it("should throw error for invalid message #3", async () => {
        expect(() => WorkerHelper.parseMessage(fakeInvalidMessage3)).to.throw(GeneralError);
    });

    it("should throw error for invalid message #4", async () => {
        expect(() => WorkerHelper.parseMessage(fakeInvalidMessage4)).to.throw(GeneralError);
    });
});

describe("WorkerHelper - generateIntervals", () => {
    it("should return one time interval if span is shorter than/equal to max duration", async () => {
        const intervals = WorkerHelper.generateIntervals(
            DateTime.fromISO("2020-01-01T00:00:00.000Z"),
            DateTime.fromISO("2020-01-01T12:00:00.000Z"),
            { hours: 12 }
        );
        expect(intervals.length).equal(1);
        expect(intervals).to.eql([
            {
                start: DateTime.fromISO("2020-01-01T00:00:00.000Z").toJSDate(),
                end: DateTime.fromISO("2020-01-01T12:00:00.000Z").toJSDate(),
            },
        ]);
    });

    it("should return multiple time intervals if span is greater than max duration", async () => {
        const intervals = WorkerHelper.generateIntervals(
            DateTime.fromISO("2020-01-01T00:00:00.000Z"),
            DateTime.fromISO("2020-01-03T01:00:00.000Z"),
            { hours: 6 }
        );
        expect(intervals.length).equal(9);
        expect(intervals[8]).to.eql({
            start: DateTime.fromISO("2020-01-03T00:00:00.000Z").toJSDate(),
            end: DateTime.fromISO("2020-01-03T01:00:00.000Z").toJSDate(),
        });
    });
});

describe("WorkerHelper - generate sub tasks", () => {
    it("should return sub tasks #1", async () => {
        const subtasks = WorkerHelper.generateSubTasksDefinitions(
            DateTime.fromISO("2020-01-01T00:00:00.000Z"),
            DateTime.fromISO("2020-01-31T23:00:00.000Z"),
            { hours: 12 },
            AggregationTaskType.WAZEJ,
            true,
            true
        );
        expect(subtasks.length).equal(62);
        expect(subtasks[61].toUnixTimestamp).equal(1580511600000);
        expect(subtasks[44].updateOnDuplicate).equal(true);
        expect(subtasks.filter((el) => el.refreshView).length).equal(62);
        expect(subtasks[23].type).equal(AggregationTaskType.WAZEJ);
    });

    it("should return sub tasks #2", async () => {
        const subtasks = WorkerHelper.generateSubTasksDefinitions(
            DateTime.fromISO("2020-01-01T00:00:00.000Z"),
            DateTime.fromISO("2020-01-01T00:32:00.000Z"),
            { minutes: 2 },
            AggregationTaskType.WAZEJ,
            true,
            false
        );
        expect(subtasks.length).equal(16);
        expect(subtasks.filter((el) => el.refreshView).length).equal(0);
    });
});
