import { AggregationWorker } from "#ie";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { DurationLike } from "@golemio/core/dist/shared/luxon";
import { Message } from "amqplib";
import * as chai from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("AggregationWorker", () => {
    let sandbox: SinonSandbox;
    let worker: AggregationWorker;
    const fakeEmptyMessage = {
        content: Buffer.from("", "utf-8"),
    } as Message;
    const fakeFullMessage = {
        content: Buffer.from(`{"from":"2022-03-10T00:00:00Z","to":"2022-03-14T00:00:00Z","type":"sddr"}`, "utf-8"),
    } as Message;
    const fakeFullMessage2 = {
        content: Buffer.from(`{"from":"2022-03-10T00:00:00Z","to":"2022-03-10T12:00:00Z","type":"sddr"}`, "utf-8"),
    } as Message;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
            })
        );
        worker = new AggregationWorker();
        sandbox.stub(worker["aggregationFactory"], "get").callsFake(() =>
            Object.assign({
                aggregateAndSave: sandbox.stub(),
                getMaxDuration: sandbox.stub().returns({ hours: 24 } as DurationLike),
            })
        );
        sandbox.stub(worker, <any>"sendMessageToExchange").returns(Promise.resolve(true));
        sandbox.spy(worker, <any>"isTaskEmpty");
        sandbox.spy(worker, <any>"isDateRangeBig");
        sandbox.spy(worker, <any>"handleEmptyTask");
        sandbox.spy(worker, <any>"processTask");
        sandbox.spy(worker, <any>"generateSubTasks");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should process empty task", async () => {
        await worker.aggregateData(fakeEmptyMessage);
        sandbox.assert.calledOnce(worker["isTaskEmpty"] as SinonSpy);
        sandbox.assert.calledOnce(worker["handleEmptyTask"] as SinonSpy);
        sandbox.assert.notCalled(worker["isDateRangeBig"] as SinonSpy);
        sandbox.assert.notCalled(worker["generateSubTasks"] as SinonSpy);
        sandbox.assert.callCount(worker["processTask"] as SinonSpy, 5);
        sandbox.assert.notCalled(worker["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.callOrder(
            worker["isTaskEmpty"] as SinonSpy,
            worker["handleEmptyTask"] as SinonSpy,
            worker["processTask"] as SinonSpy
        );
    });

    it("should generate 4 subtasks", async () => {
        await worker.aggregateData(fakeFullMessage);
        sandbox.assert.calledOnce(worker["isTaskEmpty"] as SinonSpy);
        sandbox.assert.notCalled(worker["handleEmptyTask"] as SinonSpy);
        sandbox.assert.calledOnce(worker["isDateRangeBig"] as SinonSpy);
        sandbox.assert.calledOnce(worker["generateSubTasks"] as SinonSpy);
        sandbox.assert.notCalled(worker["processTask"] as SinonSpy);
        sandbox.assert.callCount(worker["sendMessageToExchange"] as SinonSpy, 4);
        sandbox.assert.callOrder(
            worker["isTaskEmpty"] as SinonSpy,
            worker["isDateRangeBig"] as SinonSpy,
            worker["generateSubTasks"] as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should process tasks", async () => {
        await worker.aggregateData(fakeFullMessage2);
        sandbox.assert.calledOnce(worker["isTaskEmpty"] as SinonSpy);
        sandbox.assert.notCalled(worker["handleEmptyTask"] as SinonSpy);
        sandbox.assert.calledOnce(worker["isDateRangeBig"] as SinonSpy);
        sandbox.assert.notCalled(worker["generateSubTasks"] as SinonSpy);
        sandbox.assert.calledOnce(worker["processTask"] as SinonSpy);
        sandbox.assert.notCalled(worker["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.callOrder(
            worker["isTaskEmpty"] as SinonSpy,
            worker["isDateRangeBig"] as SinonSpy,
            worker["processTask"] as SinonSpy
        );
    });
});
