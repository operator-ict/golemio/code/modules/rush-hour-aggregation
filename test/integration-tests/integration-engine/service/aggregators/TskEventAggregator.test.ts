import TskEventsAggregator from "#ie/service/aggregators/TskEventsAggregator";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { Point } from "geojson";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("TskEventsAggregator", () => {
    let sandbox: SinonSandbox;
    let aggregator: TskEventsAggregator;
    const mockupQueryData = [
        {
            measured_at: "1646870400000",
            geom: { type: "Point", coordinates: [14.43416667, 50.05891667] } as Point, //POINT (14.43416667 50.05891667)
            sum: null,
            properties: { sum: null, street: "5.května(Kongresová - Táborská)", related_details: [] },
        },
    ];

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
                query: sandbox.stub().returns(Promise.resolve(mockupQueryData)),
            })
        );

        aggregator = new TskEventsAggregator();
        sandbox.stub(aggregator["repository"], "saveData").callsFake(() => Promise.resolve());
        sandbox.stub(aggregator["repository"], <any>"createSemaphore").returns({
            tryAcquire: () => Promise.resolve(true),
            release: () => Promise.resolve(),
        });
        sandbox.spy(aggregator, <any>"aggregate");
        sandbox.spy(aggregator, <any>"save");
        sandbox.spy(aggregator["repository"], "validate");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return aggregated result", async () => {
        const result = await aggregator["aggregate"](new Date(), new Date());
        expect(result.length).equal(1);
        expect(typeof result[0].measured_at).equal("number");
    });

    it("should aggregate and save result", async () => {
        await aggregator.aggregateAndSave(new Date(), new Date(), false, true);
        sandbox.assert.calledOnce(aggregator["aggregate"] as SinonSpy);
        sandbox.assert.calledOnce(aggregator["save"] as SinonSpy);
        sandbox.assert.calledOnce(aggregator["repository"].validate as SinonSpy);
        sandbox.assert.calledOnce(aggregator["repository"].saveData as SinonSpy);
        sandbox.assert.callOrder(
            aggregator["aggregate"] as SinonSpy,
            aggregator["save"] as SinonSpy,
            aggregator["repository"].validate as SinonSpy,
            aggregator["repository"].saveData as SinonSpy
        );
    });

    it("should throw exception for invalid data", async () => {
        await expect(aggregator["repository"].validate(mockupQueryData)).to.be.rejectedWith(GeneralError);
    });
});
