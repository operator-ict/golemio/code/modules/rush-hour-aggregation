import FcdEventsAggregator from "#ie/service/aggregators/FcdEventsAggregator";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { LineString } from "geojson";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("FcdEventsAggregator", () => {
    let sandbox: SinonSandbox;
    let aggregator: FcdEventsAggregator;
    const mockupQueryData = [
        {
            source_identification: "TS25684T25685",
            measured_at: "1589211000000",
            oriented_route: {
                //LINESTRING (14.00838 49.92366, 13.98715 49.91215)
                type: "LineString",
                coordinates: [
                    [14.00838, 49.92366],
                    [13.98715, 49.91215],
                ],
            } as LineString,
            traffic_level: "level3",
            properties: {
                data_quaity: 0.75,
                input_values: 2,
            },
        },
    ];
    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
                query: sandbox.stub().returns(Promise.resolve(mockupQueryData)),
            })
        );

        aggregator = new FcdEventsAggregator();
        sandbox.stub(aggregator["repository"], "saveData").callsFake(() => Promise.resolve());
        sandbox.stub(aggregator["repository"], <any>"createSemaphore").returns({
            tryAcquire: () => Promise.resolve(true),
            release: () => Promise.resolve(),
        });
        sandbox.spy(aggregator, <any>"aggregate");
        sandbox.spy(aggregator, <any>"save");
        sandbox.spy(aggregator["repository"], "validate");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return aggregated result", async () => {
        const result = await aggregator["aggregate"](new Date(), new Date());
        expect(result.length).equal(1);
        expect(typeof result[0].measured_at).equal("number");
    });

    it("should aggregate and save result", async () => {
        await aggregator.aggregateAndSave(new Date(), new Date(), false, true);
        sandbox.assert.calledOnce(aggregator["aggregate"] as SinonSpy);
        sandbox.assert.calledOnce(aggregator["save"] as SinonSpy);
        sandbox.assert.calledOnce(aggregator["repository"].validate as SinonSpy);
        sandbox.assert.calledOnce(aggregator["repository"].saveData as SinonSpy);
        sandbox.assert.callOrder(
            aggregator["aggregate"] as SinonSpy,
            aggregator["save"] as SinonSpy,
            aggregator["repository"].validate as SinonSpy,
            aggregator["repository"].saveData as SinonSpy
        );
    });

    it("should throw exception for invalid data", async () => {
        await expect(aggregator["repository"].validate(mockupQueryData)).to.be.rejectedWith(GeneralError);
    });
});
