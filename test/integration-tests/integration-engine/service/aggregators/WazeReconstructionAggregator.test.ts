import WazeReconstructionsAggregator from "#ie/service/aggregators/WazeReconstructionsAggregator";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import * as chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { LineString } from "geojson";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("WazeReconstructionsAggregator", () => {
    let sandbox: SinonSandbox;
    let aggregator: WazeReconstructionsAggregator;
    const mockupQueryData = [
        {
            id: "af887f48118ae6e91e465574b8075d4a78936235",
            measured_at: "1646875680000",
            geom_origin: {
                //LINESTRING (14.664622 50.130048, 14.664336 50.130371)
                type: "LineString",
                coordinates: [
                    [14.664622, 50.130048],
                    [14.664336, 50.130371],
                ],
            } as LineString,
            geom_startpoint: {
                //POINT (14.664622 50.130048)
                type: "Point",
                coordinates: [14.664622, 50.130048],
            },
            properties: { uuid: "1136928708", average_vehicle_speed: 0, traffic_level: "level5" },
        },
    ];

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
                query: sandbox.stub().returns(Promise.resolve(mockupQueryData)),
            })
        );

        aggregator = new WazeReconstructionsAggregator();
        sandbox.stub(aggregator["repository"], "saveData").callsFake(() => Promise.resolve());
        sandbox.stub(aggregator["repository"], <any>"createSemaphore").returns({
            tryAcquire: () => Promise.resolve(true),
            release: () => Promise.resolve(),
        });
        sandbox.spy(aggregator, <any>"aggregate");
        sandbox.spy(aggregator, <any>"save");
        sandbox.spy(aggregator["repository"], "validate");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return aggregated result", async () => {
        const result = await aggregator["aggregate"](new Date(), new Date());
        expect(result.length).equal(1);
        expect(typeof result[0].measured_at).equal("number");
    });

    it("should aggregate and save result", async () => {
        await aggregator.aggregateAndSave(new Date(), new Date(), false, true);
        sandbox.assert.calledOnce(aggregator["aggregate"] as SinonSpy);
        sandbox.assert.calledOnce(aggregator["save"] as SinonSpy);
        sandbox.assert.calledOnce(aggregator["repository"].validate as SinonSpy);
        sandbox.assert.calledOnce(aggregator["repository"].saveData as SinonSpy);
        sandbox.assert.callOrder(
            aggregator["aggregate"] as SinonSpy,
            aggregator["save"] as SinonSpy,
            aggregator["repository"].validate as SinonSpy,
            aggregator["repository"].saveData as SinonSpy
        );
    });

    it("should throw exception for invalid data", async () => {
        await expect(aggregator["repository"].validate(mockupQueryData)).to.be.rejectedWith(GeneralError);
    });
});
