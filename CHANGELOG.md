# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.3] - 2024-10-16

### Added

-   AsyncAPI documentation ([integration-engine#262](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/262))

## [1.2.2] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.1] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.0] - 2023-12-13

### Added

-   table tmc_ltcze_roads_wgs84 - podkladová vrstva pro alikaci praha dopravni

## [1.1.9] - 2023-11-15

### Changed

-   Replace bigint timestamps ([waze-ccp#6](https://gitlab.com/operator-ict/golemio/code/modules/waze-ccp/-/issues/6))

## [1.1.8] - 2023-10-27

### Changed

-   Migration of RSD tables from the public schema([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55))

## [1.1.7] - 2023-10-23

### Added

-   Added table fcd_geos_praha_dopravni
-   Added 2 functions for inicial fill this table

## [1.1.6] - 2023-08-30

### Added

-   Semaphore to prevent deadlocks while refreshing materialized view ([praha-dopravni#19](https://gitlab.com/operator-ict/golemio/code/praha-dopravni/backend/-/issues/19))
-   data retention for last 3 months

## [1.1.5] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.4] - 2023-06-05

### Fixed

-   Error imports ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.3] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.2] - 2023-04-17

### Changed

-   Set queue type default to quorum ([code#63](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/63))

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.6] - 2022-11-03

### Added

-   Added indexes for measured_at columns in all tables.
-   Added bump job into CI pipeline

## [1.0.5] - 2022-08-24

### Fixed

-   Removed recursion in interval creation fn, pass refreshView as parameter [rush-hour-aggregation#1](https://gitlab.com/operator-ict/golemio/code/modules/rush-hour-aggregation/-/issues/1)

## [1.0.4] - 2022-07-27

### Changed

-   Lookup table fcd_geos_90 moved to fcd schema

## [1.0.3] - 2022-07-18

### Changed

-   rebranding to Praha dopravni

## [1.0.2] - 2022-05-25

### Changed

-   Separation schema of WazeCCP

## [1.0.1] - 2022-05-18

### Added

-   Migrations for materialized views to calculate timespan for aggregated data
-   Additional queue to refresh historic materialized view once in 24h. Latest mviews are refreshed while aggregating data.

### Changed

-   Schema change of fcd and ndic

## [1.0.0] - 2022-05-02

### Added

-   Create module for rush hour aggregation data

