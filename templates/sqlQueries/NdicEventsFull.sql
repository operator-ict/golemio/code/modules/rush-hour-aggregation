select
	t.situation_id,
	t.situation_record_type,
	t.situation_version_time,
    t.measured_at,
	t.geom as geom_origin,
	st_centroid(t.geom) as geom_centroid,
	st_startpoint(t.geom) as geom_startpoint,
	st_endpoint(t.geom) as geom_endpoint,
	t.situation_urgency,
	case
		when t.situation_record_type::text = 'AbnormalTraffic'::text then 'ndic_congestion'::text
		when t.situation_record_type::text = 'Accident'::text then 'ndic_accident'::text
		when t.situation_record_type::text = 'CarParks'::text then 'ndic_parkandride'::text
		when t.situation_record_type::text = 'ConstructionWorks'::text then 'ndic_reconstruction'::text
		when t.situation_record_type::text = 'DisturbanceActivity'::text then 'ndic_congestion'::text
		when t.situation_record_type::text = 'GeneralObstruction'::text then
		case
			when regexp_match(t.general_public_comment, 'nehoda'::text) is not null then 'ndic_accident'::text
			else 'ndic_warning'::text
		end
		when t.situation_record_type::text = 'MaintenanceWorks'::text then 'ndic_reconstruction'::text
		when t.situation_record_type::text = 'RoadOrCarriagewayOrLaneManagement'::text then 'ndic_reconstruction'::text
		when t.situation_record_type::text = 'WeatherRelatedRoadConditions'::text then 'ndic_weather'::text
		else 'ndic_warning'::text
	end as geom_symbol_iconimage,
	case
		when t.situation_record_type::text = 'AbnormalTraffic'::text then 'ndic_line_arrowed'::text
		when t.situation_record_type::text = 'DisturbanceActivity'::text then 'ndic_line_arrowed'::text
		else 'ndic_line_simple'::text
	end as geom_line_pattern,
	json_build_object(
        'gs', t.gs,
        'general_public_comment', t.general_public_comment,
        'situation_version', t.situation_version,
        'situation_id', t.situation_id,
        'situation_record_type', t.situation_record_type,
        'situation_record_creation_time', t.situation_record_creation_time,
        'situation_version_time', t.situation_version_time,
        'situation_urgency', t.situation_urgency,
        'source', t.source,
        'geom_length_m', round(st_length(t.geom::geography))
    ) as properties
from
	(
	select
		(date_part('epoch'::text, gs.gs) * 1000::double precision)::bigint as measured_at,
		gs.gs,
		nti.general_public_comment,
		nti.situation_version,
		nti.situation_id,
		nti.situation_record_type,
		nti.situation_record_creation_time,
		nti.situation_version_time,
		nti.situation_urgency,
		nti.source,
		st_transform(st_setsrid(st_makeline(st_point(((((nti.global_network_linear -> 'startPoint'::text) -> 'sjtskPointCoordinates'::text) ->> 'sjtskX'::text)::numeric)::double precision,
		((((nti.global_network_linear -> 'startPoint'::text) -> 'sjtskPointCoordinates'::text) ->> 'sjtskY'::text)::numeric)::double precision),
		st_point(((((nti.global_network_linear -> 'endPoint'::text) -> 'sjtskPointCoordinates'::text) ->> 'sjtskX'::text)::numeric)::double precision,
		((((nti.global_network_linear -> 'endPoint'::text) -> 'sjtskPointCoordinates'::text) ->> 'sjtskY'::text)::numeric)::double precision)),
		5514),
		4326) as geom
	from
		(select
			gs_1.gs
		from
			generate_series(
				to_timestamp(((date_part('epoch'::text, (timestamp with time zone :from)) / (60 * 30)::double precision)::bigint * 60 * 30)::double precision),
				to_timestamp(((date_part('epoch'::text, (timestamp with time zone :to)) / (60 * 30)::double precision)::bigint * 60 * 30)::double precision),
				'00:30:00'::interval) gs_1(gs)
			) gs
	join ndic.ndic_traffic_info nti on
		gs.gs >= nti.validity_overall_start_time
		and gs.gs <= nti.validity_overall_end_time
		and nti.situation_version::text = ((
		select
			max(nti2.situation_version::text) as max
		from
			ndic.ndic_traffic_info nti2
		where
			nti2.situation_id::text = nti.situation_id::text
			and nti2.situation_record_type::text = nti.situation_record_type::text
			and nti2.situation_version::text = nti.situation_version::text
			and nti2.source::text = nti.source::text
			and nti.situation_version_time <= gs.gs))) t
