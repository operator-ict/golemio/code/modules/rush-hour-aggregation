select
	w.id,
	(round(extract(epoch from w.downloaded_at)::numeric / 120.0) * 120000::numeric)::bigint as measured_at,
    st_geomfromtext(concat('LINESTRING(', regexp_replace(replace(w.line::text, ', "y"'::text, ' '::text), '[{"xy:\[\]}]'::text, ''::text, 'g'::text), ')'),
	4326) as geom_origin,
	st_startpoint(st_geomfromtext(concat('LINESTRING(', regexp_replace(replace(w.line::text, ', "y"'::text, ' '::text), '[{"xy:\[\]}]'::text, ''::text, 'g'::text), ')'),
	4326)) as geom_startpoint,
    json_build_object(
        'uuid', w.uuid,
        'average_vehicle_speed', round(w.speed_kmh::double precision)::integer,
        'traffic_level', concat('level', w.level)
    ) as properties
from
	(
	select
		wazeccp_jams.id,
        wazeccp_jams.downloaded_at,
        wazeccp_jams.line,
        wazeccp_jams.level,
		wazeccp_jams.uuid,
        wazeccp_jams.speed_kmh
	from
		waze_ccp.wazeccp_jams
	where
		wazeccp_jams.downloaded_at between $from and $to) w
where
	w.level = 5
