select
	a.source_identification,
	date_part('epoch'::text, date_trunc('minute'::text, a.measurement_or_calculation_time)) * 1000::double precision as measured_at,
	g.oriented_route,
	a.traffic_level,
	json_build_object(
        'data_quality', a.data_quality,
        'input_values', a.input_values,
        'average_vehicle_speed', a.average_vehicle_speed,
        'travel_time', a.travel_time,
        'free_flow_travel_time', a.free_flow_travel_time,
        'free_flow_speed', a.free_flow_speed
    ) as properties
from
	fcd.fcd_traff_params_part a
join praha_dopravni.fcd_geos_praha_dopravni g on
	a.source_identification::text = g.source_identification::text
where
	a.measurement_or_calculation_time between $from and $to
