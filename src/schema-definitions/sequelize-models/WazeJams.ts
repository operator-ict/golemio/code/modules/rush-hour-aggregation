import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { Geometry } from "geojson";
import IWazeJams from "./interfaces/IWazeJams";

export default class WazeJams extends Model<WazeJams> implements IWazeJams {
    declare id: string;
    declare measured_at: number;
    declare geom_origin: Geometry;
    declare geom_startpoint: Geometry;
    declare traffic_level: string;
    declare properties: object;

    public static attributeModel: ModelAttributes<WazeJams> = {
        id: { type: DataTypes.STRING, primaryKey: true },
        measured_at: { type: DataTypes.BIGINT },
        geom_origin: { type: DataTypes.GEOMETRY },
        geom_startpoint: { type: DataTypes.GEOMETRY },
        traffic_level: { type: DataTypes.STRING },
        properties: { type: DataTypes.JSON },
    };

    public static arrayJsonSchema: JSONSchemaType<IWazeJams[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                measured_at: { type: "number" },
                geom_origin: { $ref: "#/definitions/geometry" },
                geom_startpoint: { $ref: "#/definitions/geometry" },
                traffic_level: { type: "string" },
                properties: { type: "object" },
            },
            required: ["id"],
        },
        definitions: {
            // @ts-expect-error since it is referenced definition from other file ts doesnt like it.
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
