import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { Geometry } from "geojson";
import IWazeReconstructions from "./interfaces/IWazeReconstructions";

export default class WazeReconstructions extends Model<WazeReconstructions> implements IWazeReconstructions {
    declare id: string;
    declare measured_at: number;
    declare geom_origin: Geometry;
    declare geom_startpoint: Geometry;
    declare properties: object;

    public static attributeModel: ModelAttributes<WazeReconstructions> = {
        id: { type: DataTypes.STRING, primaryKey: true },
        measured_at: { type: DataTypes.BIGINT },
        geom_origin: { type: DataTypes.GEOMETRY },
        geom_startpoint: { type: DataTypes.GEOMETRY },
        properties: { type: DataTypes.JSON },
    };

    public static arrayJsonSchema: JSONSchemaType<IWazeReconstructions[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                measured_at: { type: "number" },
                geom_origin: { $ref: "#/definitions/geometry" },
                geom_startpoint: { $ref: "#/definitions/geometry" },
                properties: { type: "object" },
            },
            required: ["id"],
        },
        definitions: {
            // @ts-expect-error since it is referenced definition from other file ts doesnt like it.
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
