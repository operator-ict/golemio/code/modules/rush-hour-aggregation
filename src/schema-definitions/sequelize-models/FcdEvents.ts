import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions/SharedSchemaProvider";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { Geometry } from "geojson";
import IFcdEvent from "./interfaces/IFcdEvent";
import IWazeJams from "./interfaces/IWazeJams";

export default class FcdEvent extends Model<FcdEvent> implements IFcdEvent {
    declare source_identification: string;
    declare measured_at: number;
    declare oriented_route: Geometry;
    declare traffic_level: string;
    declare properties: object;

    public static attributeModel: ModelAttributes<FcdEvent> = {
        source_identification: { type: DataTypes.STRING, primaryKey: true },
        measured_at: { type: DataTypes.BIGINT, primaryKey: true },
        oriented_route: { type: DataTypes.GEOMETRY },
        traffic_level: { type: DataTypes.STRING },
        properties: { type: DataTypes.JSON },
    };

    public static arrayJsonSchema: JSONSchemaType<IWazeJams[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                source_identification: { type: "string" },
                measured_at: { type: "number" },
                traffic_level: { type: "string" },
                oriented_route: { $ref: "#/definitions/geometry" },
                properties: { type: "object" },
            },
            required: ["source_identification", "measured_at"],
        },
        definitions: {
            // @ts-expect-error since it is referenced definition from other file ts doesnt like it.
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
