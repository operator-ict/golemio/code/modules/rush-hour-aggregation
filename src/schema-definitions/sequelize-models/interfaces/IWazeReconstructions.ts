import { Geometry } from "geojson";

export default interface IWazeReconstructions {
    id: string;
    measured_at: number;
    geom_origin: Geometry;
    geom_startpoint: Geometry;
    properties: object;
}
