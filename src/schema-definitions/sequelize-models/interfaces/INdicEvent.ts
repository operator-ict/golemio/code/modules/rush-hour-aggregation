import { Geometry } from "geojson";

export default interface ITskEvent {
    situation_id: string;
    situation_record_type: string;
    situation_version_time: string;
    measured_at: number;
    geom_origin: Geometry;
    geom_centroid: Geometry;
    geom_startpoint: Geometry;
    geom_endpoint: Geometry;
    situation_urgency: string;
    geom_symbol_iconimage: string;
    geom_line_pattern: string;
    properties: object;
}
