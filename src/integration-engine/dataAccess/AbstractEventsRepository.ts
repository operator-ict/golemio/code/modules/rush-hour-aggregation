import PrahaDopravniInfo from "#sch/sequelize-models/PrahaDopravniInfo";
import { IModel, PostgresModel, RedisConnector } from "@golemio/core/dist/integration-engine";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { Mutex } from "@golemio/core/dist/shared/redis-semaphore";

export default abstract class EventsRepository<T> extends PostgresModel implements IModel {
    private readonly lockTimeout = 4 * 60 * 60 * 1000; // 4 hours

    protected abstract materializedViewNameLatest: string;
    protected abstract materializedViewNameHistoric: string;

    abstract saveData(data: T[], updateOnDuplicate: boolean): Promise<void>;

    public refreshLatestMaterializedView = async (): Promise<void> => {
        await this.refreshMaterializedView(this.materializedViewNameLatest);
    };

    public refreshHistoricMaterializedView = async (): Promise<void> => {
        await this.refreshMaterializedView(this.materializedViewNameHistoric);
    };

    private refreshMaterializedView = async (viewName: string): Promise<void> => {
        log.debug(`Refreshing materialized view: ${viewName}`);

        const semaphore = this.createSemaphore(this.materializedViewNameLatest);
        const lockAcquired = await semaphore.tryAcquire();
        if (!lockAcquired) {
            log.info(`Rush Hour Aggregation - unable to obtain mutex lock for: ${viewName}`);
            return;
        }

        try {
            const sql = `REFRESH MATERIALIZED VIEW CONCURRENTLY ${PrahaDopravniInfo.SCHEMA}.${viewName}`;

            await this.query(sql);
        } finally {
            await semaphore.release();
        }
    };

    private createSemaphore(keyPhrase: string) {
        const redisClient = RedisConnector.getConnection();
        return new Mutex(redisClient, `RushHourAggregation-${keyPhrase}`, {
            acquireAttemptsLimit: 1,
            lockTimeout: this.lockTimeout,
            refreshInterval: this.lockTimeout * 0.8,
            onLockLost: (err) => {
                log.warn(`RushHourAggregation - ${err.message}`);
            },
        });
    }
}
