import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import NdicEvent from "#sch/sequelize-models/NdicEvent";
import PrahaDopravniInfo from "#sch/sequelize-models/PrahaDopravniInfo";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import EventsRepository from "./AbstractEventsRepository";

export default class NdicEventsRepository extends EventsRepository<NdicEvent> {
    protected materializedViewNameLatest = "ndic_events_full_snapshots_latest";
    protected materializedViewNameHistoric = "ndic_events_full_snapshots_history";

    constructor() {
        super(
            "NdicEventsRepository",
            {
                outputSequelizeAttributes: NdicEvent.attributeModel,
                pgTableName: PrahaDopravniInfo.TABLE_NAMES[AggregationTaskType.NDIC],
                pgSchema: PrahaDopravniInfo.SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("NdicEventsRepository", NdicEvent.arrayJsonSchema, true)
        );
    }

    public saveData = async (data: NdicEvent[], updateOnDuplicate: boolean) => {
        await this["sequelizeModel"].bulkCreate<NdicEvent>(data, {
            ignoreDuplicates: !updateOnDuplicate,
            returning: false,
            updateOnDuplicate: updateOnDuplicate ? ["properties"] : undefined,
        });
    };
}
