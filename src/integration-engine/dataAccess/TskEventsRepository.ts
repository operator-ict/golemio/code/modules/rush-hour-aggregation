import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import PrahaDopravniInfo from "#sch/sequelize-models/PrahaDopravniInfo";
import TskEvent from "#sch/sequelize-models/TskEvent";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import EventsRepository from "./AbstractEventsRepository";

export default class TskEventsRepository extends EventsRepository<TskEvent> {
    protected materializedViewNameLatest = "tsk_std_last_30min_snapshots_latest";
    protected materializedViewNameHistoric = "tsk_std_last_30min_snapshots_history";

    constructor() {
        super(
            "TskEventsRepository",
            {
                outputSequelizeAttributes: TskEvent.attributeModel,
                pgTableName: PrahaDopravniInfo.TABLE_NAMES[AggregationTaskType.SDDR],
                pgSchema: PrahaDopravniInfo.SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("TskEventsRepository", TskEvent.arrayJsonSchema, true)
        );
    }

    public saveData = async (data: TskEvent[], updateOnDuplicate: boolean) => {
        await this["sequelizeModel"].bulkCreate<TskEvent>(data, {
            ignoreDuplicates: !updateOnDuplicate,
            returning: false,
            updateOnDuplicate: updateOnDuplicate ? ["sum", "properties"] : undefined,
        });
    };
}
