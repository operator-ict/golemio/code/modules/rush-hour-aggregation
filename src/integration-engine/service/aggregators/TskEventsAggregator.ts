import AggregationRepository from "#ie/dataAccess/AggregationRepository";
import TskEventsRepository from "#ie/dataAccess/TskEventsRepository";
import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import ITskEvent from "#sch/sequelize-models/interfaces/INdicEvent";
import TskEvent from "#sch/sequelize-models/TskEvent";
import { DateTime, DurationLike } from "@golemio/core/dist/shared/luxon";
import AbstractAggregator from "./AbstractAggregator";

export default class TskEventsAggregator extends AbstractAggregator<TskEvent> {
    protected override maxAggregationDuration: DurationLike = { hours: 2 };
    protected repository: TskEventsRepository;

    constructor() {
        super();
        this.repository = new TskEventsRepository();
    }

    protected aggregate = async (from: Date, to: Date): Promise<TskEvent[]> => {
        const result = await AggregationRepository.getInstance().aggregationQuery<ITskEvent>(
            AggregationTaskType.SDDR,
            undefined,
            {
                from: DateTime.fromJSDate(from).toISO(),
                to: DateTime.fromJSDate(to).toISO(),
            }
        );

        return this.mapToDto(result);
    };

    protected mapToDto(data: any[]): TskEvent[] {
        return data.map((element) => {
            return {
                measured_at: Number.parseInt(element.measured_at),
                geom: element.geom,
                sum: element.sum && Number.isInteger(element.sum) ? Number.parseInt(element.sum) : null,
                properties: element.properties,
            } as TskEvent;
        });
    }
}
