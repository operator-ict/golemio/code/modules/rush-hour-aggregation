import { DurationLike } from "@golemio/core/dist/shared/luxon";

export default interface IAbstractAggregator {
    aggregateAndSave(from: Date, to: Date, updateOnDuplicate: boolean, refreshView: boolean): Promise<void>;
    getMaxDuration(): DurationLike;
    refreshHistoricMaterialView(): Promise<void>;
}
