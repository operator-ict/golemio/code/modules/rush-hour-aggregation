import AbstractEventsRepository from "#ie/dataAccess/AbstractEventsRepository";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { DurationLike } from "@golemio/core/dist/shared/luxon";
import IAbstractAggregator from "./interfaces/IAbstractAggregator";

export default abstract class AbstractAggregator<T> implements IAbstractAggregator {
    protected maxAggregationDuration: DurationLike = { hours: 6 };
    protected abstract repository: AbstractEventsRepository<T>;

    public aggregateAndSave = async (from: Date, to: Date, updateOnDuplicate: boolean, refreshView: boolean): Promise<void> => {
        const aggregationResult = await this.aggregate(from, to);
        log.debug(
            `Saving aggregate ${this.constructor.name} from: ${from.toISOString()} to: ${to.toISOString()} count: ${
                aggregationResult.length
            }`
        );
        await this.save(aggregationResult, updateOnDuplicate, refreshView);
    };

    public getMaxDuration(): DurationLike {
        return this.maxAggregationDuration;
    }

    public refreshHistoricMaterialView = async () => {
        await this.repository.refreshHistoricMaterializedView();
    };

    protected abstract aggregate(from: Date, to: Date): Promise<T[]>;

    protected save = async (data: T[], updateOnDuplicate: boolean, refreshView: boolean): Promise<void> => {
        await this.repository.validate(data);
        await this.repository.saveData(data, updateOnDuplicate);
        if (refreshView) {
            await this.repository.refreshLatestMaterializedView();
        }
    };
}
