import AggregationRepository from "#ie/dataAccess/AggregationRepository";
import WazeReconstructionsRepository from "#ie/dataAccess/WazeReconstructionsRepository";
import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import IWazeReconstructions from "#sch/sequelize-models/interfaces/IWazeReconstructions";
import WazeReconstructions from "#sch/sequelize-models/WazeReconstructions";
import { DurationLike } from "@golemio/core/dist/shared/luxon";
import AbstractAggregator from "./AbstractAggregator";

export default class WazeReconstructionsAggregator extends AbstractAggregator<WazeReconstructions> {
    protected override maxAggregationDuration: DurationLike = { hours: 2 };
    protected repository: WazeReconstructionsRepository;

    constructor() {
        super();
        this.repository = new WazeReconstructionsRepository();
    }

    protected aggregate = async (from: Date, to: Date): Promise<WazeReconstructions[]> => {
        const result = await AggregationRepository.getInstance().aggregationQuery<IWazeReconstructions>(
            AggregationTaskType.WAZER,
            { from, to }
        );

        return this.mapToDto(result);
    };

    protected mapToDto(data: any[]): WazeReconstructions[] {
        return data.map((element) => {
            return {
                id: element.id,
                measured_at: Number.parseInt(element.measured_at),
                geom_origin: element.geom_origin,
                geom_startpoint: element.geom_startpoint,
                properties: element.properties,
            } as WazeReconstructions;
        });
    }
}
