import { AggregationTaskType } from "#sch/helpers/AggregationTaskType";
import IAggregationTask from "./interfaces/IAggregationTask";

export default class AggregationTask implements IAggregationTask {
    public from?: Date;
    public to?: Date;
    public type?: AggregationTaskType;
    public updateOnDuplicate: boolean;
    public refreshView: boolean;

    public get fromUnixTimestamp(): Number | undefined {
        return this.from ? this.from.getTime() : undefined;
    }

    public get toUnixTimestamp(): Number | undefined {
        return this.to ? this.to.getTime() : undefined;
    }

    constructor(from: Date, to: Date, type: AggregationTaskType, updateOnDuplicate?: boolean, refreshView?: boolean) {
        this.from = from;
        this.to = to;
        this.type = type;
        this.updateOnDuplicate = updateOnDuplicate || false;
        this.refreshView = refreshView || false;
    }
}
