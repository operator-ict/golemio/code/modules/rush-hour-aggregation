/* Replace with your SQL commands */

CREATE TABLE waze_jams (
	id varchar(40) NOT NULL,
    measured_at int8 NOT NULL,
    geom_origin geometry NULL,
	geom_startpoint geometry NULL,
	traffic_level text NULL,
    properties json NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,
    CONSTRAINT waze_jams_pk PRIMARY KEY (id)
);


CREATE TABLE waze_reconstructions (
	id varchar(40) NOT NULL,
    measured_at int8 NOT NULL,
	geom_origin geometry NULL,
	geom_startpoint geometry NULL,
    properties json NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,
    CONSTRAINT waze_reconstructions_pk PRIMARY KEY (id)
);

CREATE TABLE fcd_events (
	source_identification varchar(50) NOT NULL,
    measured_at int8 NOT NULL,
    oriented_route geometry NULL,
    traffic_level varchar(50) NULL,
    properties json NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,
    CONSTRAINT fcd_events_pk PRIMARY KEY (source_identification, measured_at)
);

CREATE TABLE tsk_std_last_30min (
    measured_at int8 NOT NULL,
    geom geometry NULL,
    sum int8 NULL,
    properties json NULL,
    "updated_at" timestamptz NOT NULL,
    "created_at" timestamptz NOT NULL,
    CONSTRAINT tsk_std_last_30min_pk PRIMARY KEY (measured_at, geom)
);

CREATE TABLE ndic_events_full (
    situation_id varchar(100) NOT NULL,
	situation_record_type varchar(100) NOT NULL,
    situation_version_time timestamptz NOT NULL,
    measured_at int8 NOT NULL,
    geom_origin  geometry NULL,
    geom_centroid  geometry NULL,
    geom_startpoint geometry NULL,
    geom_endpoint  geometry NULL,
    situation_urgency varchar(100) NULL,
    geom_symbol_iconimage text NULL,
    geom_line_pattern text NULL,
    properties json NULL,
    updated_at timestamptz NOT NULL,
    created_at timestamptz NOT NULL,
    CONSTRAINT ndic_events_full_pk PRIMARY KEY (situation_id, situation_record_type, situation_version_time, measured_at)
);
