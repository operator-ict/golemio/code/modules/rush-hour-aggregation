CREATE OR REPLACE PROCEDURE data_retention()
 LANGUAGE plpgsql
 SET search_path FROM CURRENT
AS $procedure$
    declare
        olderThanEpoch int8;
    begin
        select extract(epoch from (current_date - interval '3 months')) * 1000 into olderThanEpoch;
        delete from fcd_events where measured_at < olderThanEpoch;
        delete from ndic_events_full where measured_at < olderThanEpoch;
        delete from tsk_std_last_30min where measured_at < olderThanEpoch;
        delete from waze_jams where measured_at < olderThanEpoch;
        delete from waze_reconstructions where measured_at < olderThanEpoch;
    end
$procedure$;